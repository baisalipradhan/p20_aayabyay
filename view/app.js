
//budget controller
var budgetController = (function(){

    var Expense = function(id, description, value){

        this.id=id;
        this.description=description;
        this.value=value;

    };

    var Income = function(id, description, value){

        this.id=id;
        this.description=description;
        this.value=value;

    }; 
        
    var data = {
        allItems: {
            exp: [],
            inc: []
        },

        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1

    };

    var calculateTotal = function(type){
        var sum=0;
        data.allItems[type].forEach(function(cur){
            sum = sum + cur.value;
        });
        data.totals[type] = sum;
    }


    return{
        
        
        addItem: function(type, des, val){





     //       console.log('receive additem');
            var newItem, ID;

            // Create new ID
            if (data.allItems[type].length > 0){
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            }
            else{
                ID = 0;
            }

           // Create a new item based on 'exp' or 'inc'

            if(type === 'exp')
            {
                console.log('expense');
                newItem = new Expense(ID, des, val);
            }
            else if (type === 'inc'){
                newItem = new Income(ID, des, val);
                console.log('income');
            }

            //adding item to the data structure
            data.allItems[type].push(newItem);
            return newItem;

        },
        cleare : function()
        {
            data.allItems['exp'] = [];
            data.allItems['inc'] = [];
        },

        deleteItem: function(type, id){

            var ids, index;
            
            // id = 6
            //data.allItems[type][id];
            // ids = [1 2 4  8]
            //index = 3
            
            ids = data.allItems[type].map(function(current) {
                return current.id;
            });

            index = ids.indexOf(id);

            if (index !== -1) {
                data.allItems[type].splice(index, 1);   // deleting data from array
            }
            
        },

        claculateBudget: function(){

            // calculate total income and expenses


            calculateTotal('exp');
            calculateTotal('inc');

            // calculate the budget: income - expences

            data.budget=data.totals.inc - data.totals.exp;


            // calculate the percentage og income that we spent
            if(data.totals.inc > 0){
                data.percentage = Math.round ((data.totals.exp / data.totals.inc) * 100) ;
            }
            else
            {
                data.percentage = -1;
            }


        },
        getBudget : function(){

            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        },

        testing : function(){
            console.log(data);
        }

    };

})();


// UI controller
var UIcontroller = (function(){

    var DOMstring = {
        inputType : '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn1',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container'
    };

    return {
        getInput: function()
        {
            
          return{
            type: document.querySelector(DOMstring.inputType).value,  // will be either inc or exp
            description: document.querySelector(DOMstring.inputDescription).value,
            value: parseFloat(document.querySelector(DOMstring.inputValue).value)
          };
        },

        addListItem: function(obj, type){

            var html, newHtml, element;
            //create HTML string with paceholder text
            console.log(type);

            if(type === 'inc')
            {
                element = DOMstring.incomeContainer;
                html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
                console.log(element);
            }

            else if (type === 'exp')
            {
                element = DOMstring.expensesContainer;
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div></div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
                console.log(element);
            }

            //Replace the placeholder text with some actual data

            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', obj.value);

            // insert the html into the DOM

            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        deleteListItem: function(selectorID){

            var el = document.getElementById(selectorID);
            el.parentNode.removeChild(el);

        },

        clearFields: function(){

            var fields, fieldsArr;

            fields = document.querySelectorAll(DOMstring.inputDescription + ',' + DOMstring.inputValue);
            fieldsArr=Array.prototype.slice.call(fields);

            fieldsArr.forEach(function(cur) {  //Revisit for Better UnderStand
                
                cur.value = "";
            });


        },
        displayBudget: function(obj){
            document.querySelector(DOMstring.budgetLabel).textContent = obj.budget;
            document.querySelector(DOMstring.incomeLabel).textContent = obj.totalInc;
            document.querySelector(DOMstring.expensesLabel).textContent = obj.totalExp;
            

            if(obj.percentage > 0){
                document.querySelector(DOMstring.percentageLabel).textContent = obj.percentage;
            }
            else{
                document.querySelector(DOMstring.percentageLabel).textContent = '----';
            }

        },

        getDOMstring : function()
        {
            return DOMstring;
        }
    };

})();




//Global App Controller
var controller = (function(budgetCtrl, UICtrl){


    var setupEventListners = function(){
        var DOM = UICtrl.getDOMstring();
        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);

        document.addEventListener('keypress', function(event){
            if(event.keyCode == 13 || event.which == 14)
            {
                
                ctrlAddItem();
            }
        });

        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);
    };


    var updateBudget = function(){
        // 1. Calculate the budget

        budgetCtrl.claculateBudget();


        // 2. Return the budget

        var budget = budgetCtrl.getBudget();

        
        // 3. Display the budget on the UI

        UICtrl.displayBudget(budget);

    }

    
    var ctrlAddItem = function(){

        var input, newItem;

        // 1. get the field input data


        input = UICtrl.getInput();


    //    if(input.description !== "" && !isNaN(input.value) && input.value > 0 )
    //    {
            // 2. Add the item to the budger controller  


            var month = 'January';
            var result = document.querySelector('#demo');
            result.textContent = 'You select January';
            fetch('http://localhost:8000/app?month='+month).then((response) => {

                response.json().then((data) => {
                    if (data.error) {
                        console.log("data error");
                    } else {
                    
                      //  console.log(data.length);
                        data.forEach(function(item, index, arr){
                        //    console.log(data[index]);
                        //    console.log("calling additem");
                            newItem = budgetCtrl.addItem(data[index].type, data[index].description, data[index].value);
                            UICtrl.addListItem(newItem, data[index].type);
                            UICtrl.clearFields();
                            updateBudget();
    
                        })
                    }
                })
            })
    
            
            
            const selectElement = document.querySelector('.month__type');
            selectElement.addEventListener('change', (event) => {
                        result = document.querySelector('#demo');
                        result.textContent = `You select ${event.target.value}`;
                        month = event.target.value;

                        
            // To cleare the existing data


            // console.log(data.allItems['exp'].length);
            // console.log(data.allItems['inc'].length);
            //   data.allItems['exp'] = [];
            //   data.allItems['inc'] = [];
            budgetController.cleare();

            const income = document.querySelector(".income__list");
            income.innerHTML = '';
            const expense = document.querySelector(".expenses__list");
            expense.innerHTML = ''; 





            // End of the section to cleare the existing data

                 //       controller.init();
                        fetch('http://localhost:8000/app?month='+month).then((response) => {

                            response.json().then((data) => {
                                if (data.error) {
                                    console.log("data error");
                                } else {
                                


                                  //  console.log(data.length);
                                    data.forEach(function(item, index, arr){
                                    //    console.log(data[index]);
                                    //    console.log("calling additem");
                                        newItem = budgetCtrl.addItem(data[index].type, data[index].description, data[index].value);
                                        UICtrl.addListItem(newItem, data[index].type);
                                        UICtrl.clearFields();
                                        updateBudget();
                
                                    })
                                }
                            })
                        })



            });






    //        newItem = budgetCtrl.addItem(input.type, input.description, input.value);



            // 3. Add the item to the UI

//            UICtrl.addListItem(newItem, input.type);

            // 4. Cleare the fields

 //           UICtrl.clearFields();

            // 5. Calculate and update budget

//            updateBudget();


  //      }
    };

    var ctrlDeleteItem = function(event) {
        var itemID, type, ID;
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
        if (itemID)
        {
            //inc-1
            splitID = itemID.split('-');
            type = splitID[0];
            ID = parseInt(splitID[1]);

            // 1. Delete the item from data structure

            budgetCtrl.deleteItem(type, ID);

            // 2. Delete the item from the UI

            UICtrl.deleteListItem(itemID);

            // 3. Update and show the new budget
            updateBudget();


        }
    }

   
    return {
        init: function(){

         //   console.log('Application has started');
            updateBudget(); 
            setupEventListners();
            ctrlAddItem();
        }
    };


})(budgetController, UIcontroller);


controller.init();


