
var yearlyIncome = '#yearlyIncome';
var yearlyExpense ='#yearlyExpense';
var yearlyBudget = '#yearlyBudget';
var monthlyIncome = '#monthlyIncome';
var monthlyExpense = '#monthlyExpense'
var monthlyBudget = '#monthlyBudget';
var profileName = '#profileName';
var yrInc = 0;
var yrExp = 0;
var monthType = '.monthType';
var monthDetails = '#monthDetails';

document.querySelector(yearlyBudget).textContent =0; 
document.querySelector(monthlyIncome).textContent =0; 
document.querySelector(monthlyExpense).textContent =0; 
document.querySelector(monthlyBudget).textContent =0; 
document.querySelector(yearlyIncome).textContent =yrInc; 
document.querySelector(yearlyExpense).textContent =yrExp;
document.querySelector(yearlyBudget).textContent = yrInc - yrExp;
//yearData();

fetch('http://localhost:8000/userDetails').then((response) => {

    response.json().then((data) => {
        if (data.error) {
            console.log("data error");
        } else {
        
         var name = data[0].fname + ' ' + data[0].lname;
         document.querySelector(profileName).textContent = name;

        }
    })
});

function yearData(){
    yrInc = 0;
    yrExp = 0;

    fetch('http://localhost:8000/getyearlyData').then((response) => {

    response.json().then((data) => {
        if (data.error) {
            console.log("data error");
        } else {
        
            
            data.forEach(function(item, index, arr){


                
                if(data[index].type == "inc"){
                
                    yrInc = yrInc + data[index].value;
                
                }
                else
                {
                    yrExp = yrExp + data[index].value;
                }
            
            
        })
        document.querySelector(yearlyIncome).textContent =yrInc; 
        document.querySelector(yearlyExpense).textContent =yrExp;
        document.querySelector(yearlyBudget).textContent = yrInc - yrExp;

    }
    })
});

}


function monthData(month ){

    fetch('http://localhost:8000/getMonthlyData?month='+month).then((response) => {

                response.json().then((data) => {
                    if (data.error) {
                        console.log("data error");
                    } else {
                    
                        var monInc = 0;
                        var monExp = 0;

                        var e = document.querySelector('.tbody'); 
                        //e.firstElementChild can be used. 
                        var child = e.lastElementChild;  
                         while (child) { 
                             e.removeChild(child); 
                            child = e.lastElementChild; 
                        } 

                        var incHtml = '<tr><th scope="row" class="income">Income_Description</th><th scope="row">3746</th><th scope="row"><button type="button" class="btn btn1">Income</button></th></tr>';
                        var expHtml = '<tr><th scope="row"  >Expense_Description</th><th scope="row">3746</th><th scope="row"><button type="button" class="btn btn2">Expense</button></th></tr>';


                        data.forEach(function(item, index, arr){

                            console.log(data[index].type);
                            
                        if(data[index].type == "inc"){


                            monInc = monInc + data[index].value;
                            var newIncHtml = incHtml.replace('Income_Description', data[index].description);
                            newIncHtml = newIncHtml.replace('3746', data[index].value);
                            document.querySelector('.tbody').insertAdjacentHTML('beforeend', newIncHtml);
                        }    
                        else{

                            monExp = monExp + data[index].value;
                            var newExpHtml = expHtml.replace('Expense_Description', data[index].description);
                            newExpHtml = newExpHtml.replace('3746', data[index].value);
                            document.querySelector('.tbody').insertAdjacentHTML('beforeend', newExpHtml);

                        }

    
                        })

                        document.querySelector(monthlyIncome).textContent =monInc; 
                        document.querySelector(monthlyExpense).textContent =monExp; 
                        document.querySelector(monthlyBudget).textContent =monInc - monExp; 

                    }
                })
            })
            yearData();

}

var month = 'January';
monthData(month);



const selectElement = document.querySelector(monthType);
selectElement.addEventListener('change', (event) => {
            result = document.querySelector(monthDetails);
            result.textContent = `Details of ${event.target.value}`;
            month = event.target.value;
            monthData(month);

})



