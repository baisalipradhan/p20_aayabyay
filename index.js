var connection = require('./db/database');
var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var session = require('express-session');


var app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));


app.use(express.static(path.join(__dirname, '/')));
app.use(express.static(path.join(__dirname, 'view')));

console.log(__dirname, '/');

app.get('/', function (request, response) {
    response.sendFile(path.join(__dirname + '/view/login.html'));
});


app.post('/auth', function(request, response) {
	var username = request.body.userName;
	var password = request.body.pwd;
	
//	console.log(username +"  " + password);
	if (username && password) {
		
		connection.query('SELECT * FROM users WHERE userName = ? AND password = ?', [username, password], function(error, results, fields) {
			if (results.length > 0) {
				request.session.loggedin = true;
				request.session.username = username;
				request.session.userId = results[0].uid;

			//	console.log(results[0].uid);
			
            	response.redirect('/dashBoard');
            //    response.send('Correct userName and Password');
			} else {
				response.send('Incorrect Username and/or Password!');
			}			
			response.end();
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}
});

app.get('/dashBoard', function(request, response){
    response.sendFile(path.join(__dirname + '/view/dashboard.html'));
});


app.post('/regis', function(req, res){

         var firstName = req.body.firstname;
         var lastName = req.body.lastname;
		 var Email = req.body.email;
		 var phone = req.body.phoneNumber;
		 var username = req.body.userName;
         var password = req.body.pwd;
	  
        var sql = 'insert into users(fname, lname, email, phoneNumber, userName, password) values(?, ?, ?, ?, ?, ? )';

        connection.query(sql,  [firstName, lastName, Email,phone, username, password, ], function (err, result) {
            if (err) throw err; 
		//	res.send('Data Added');
			res.redirect('/');
         });

        // })

	});



app.post('/addData', function (request, response) {

    const userId = request.session.userId;
    const month = request.body.Select1;
    const monthId =1;
    const type =  request.body.type;
    const description = request.body.description;
    const value = request.body.value;


    var sql = 'insert into budgetDash(userId, month, monthId, type, description, value) values(?, ?, ?, ?, ?, ? )';
    connection.query(sql, [userId, month, monthId, type, description, value], function(err, result){
        if(err) throw err;
        else
     //   response.send("Data Added");
        
        response.redirect('/dashBoard');
    })

});

app.get('/getMonthlyData', function(request, response){


    var month = request.query.month;
//    console.log(month);
//    console.log("chinmaya");
    var sql = `select * from budgetDash where userId = ${request.session.userId} and month = '${month}'`;
  //  var sql = 'select * from budgetDash where userId = 2';
    connection.query(sql, function(err, result){
        if(err) throw err;
        else
        response.send(result);
    })


//    var s = 

});



app.get('/getYearlyData', function(request, response){

    var sql = 'select type, value from budgetDash where userId = '+ request.session.userId;
    connection.query(sql, function(err, result){
        if(err) throw err;
        else
        response.send(result);
    })

});

app.get('/userDetails', function(request, response){

    var sql = 'select * from users where uid = '+request.session.userId;
    connection.query(sql, function(err, result){
        if(err) throw err;
        
        else
        response.send(result);
    })


});

// app.get('/app', function (request, response) {

//     // if (!request.query.month)
//     //     response.send("error");

//     // const month = request.query.month;
//     // var sql = 'select * from bud where month = ' + month;

//  //   console.log(request.query.month);
//   //  const month = request.query.month;

//   // var sql = `select * from bud where month = '${month}'`;
//   // console.log(sql);
//     //var sql = 'select * from bud';
//     connection.query(sql, function (err, result) {
//         response.send(result);

//     })
// })


app.listen(8000, function () {
    console.log("server is running at localhost:8000")
});